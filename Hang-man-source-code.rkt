;; hang-man for REPL Scheme


;input file - sources of word for hangman
(define source-name "glossary.txt")

;; Side effect:
;; String-> IO([String])
;; Passed the path, open the file containing glossary
(define (read-words-from filename)
  (let* ((port (open-input-file filename))
         (res (read-word-list port '())))
    (close-input-port port)
    res))

;; Side effect
;; Fd -> [String] -> IO ([String])
;; Passed port and acumulator, return the all the words as strings
(define (read-word-list port acc)
  (let ((stuff (read port)))
    (if (eof-object? stuff)
        acc
        (read-word-list port
                        (cons (symbol->string stuff) acc)))))

(define list-of-words (read-words-from source-name))

;Kausar & Mark
;; STATE OF THE GAME
(define glossary(map string->list list-of-words));contains the list of words

(define word-to-guess
  (list-ref glossary(random(length glossary))));pulling out the random word from glossary

(define total-hits (length word-to-guess)); whatver length of word is pulled, it will have the same value
(define partial-sol
  (string->list (make-string total-hits #\*)));; transforming the word to *
(define hits 0)
(define plays 0)
(define failures 0)
(define total-failures 6)

;; IO(String)
;Resets everything to
; Displays the string once the game has been beaten
(define (completed-game-status)
  (begin
    (format "~a H:~a/~a F:~a/~a ~a ~a ~a"
            (list->string partial-sol)
            hits  total-hits
            failures  total-failures
            "No of plays :" plays
            (if (and
                 (< hits total-hits)
                 (< failures total-failures))
                ""
                 (string-append "Gameover " (list->string word-to-guess))))))


;;  PURELY FUNCTIONAL
; Kausar 
;; F1 - I solution
(define (occurrences word letter)
  (cond
    [(null? word)0];;null? = condition, 0 is the return value
    [(equal? (car word) letter) (add1 (occurrences(cdr word) letter))]
    ;;char is equal to the first element of word,
    ;;if yes recursive call with the remaining list and add 1 to the count
    ;; Else Block below
    [else
     (occurrences(cdr word) letter);;
     ;;this is the recursive call that takes remaining elem
     ;;if the first element is not equal to the char given
     ]
    )
  )

;; F1 - ii solution
(define (indices word letter)
 (let foo ((word word);; given word
           (cont '());;index containers
           (index 0)); Index seed
  (cond
    [(empty? word) (reverse cont)];once the loop finishes, reverse indexes to return the right value
    [(equal? (car word)letter)
     (foo(cdr word)
         (cons index cont); put it in the list by constructing it as a pair 
         (add1 index))];add 1 to index and loop for the remaining value inside the list
    [else
     (foo(cdr word); loops for the remaining elements
         cont
         (add1 index))]
    )
   )
  )

;; F1 - iii
(define (noOfHits word)
    (length; transform the true values into numeric value
     (filter (λ (hits); filter out true values from this arg
               (not (equal? hits #\*)))word)));return values that are not equal to * 

;F1 IV
(define (replace-indices word idx new)
  (if (or (null? word)(null? idx))word ;Return the list if index or word is empty
      (let vars((given word); given word
                (rem-elem (cdr word)); remaining elements
                (first-elem (car word)); replacement value
                (temp '()); temp container used to reverse
                (index idx);indexes tail
                (cont '()));demecrementing index container
        (cond
          ((null? index)
           
           (if (null? rem-elem)
               (reverse (cons first-elem temp)); the loop finishes
               ; takes the remaining values and loops them 
               (vars rem-elem (cdr rem-elem)(car rem-elem)(cons first-elem temp)
                     cont '())))
          
          ((zero?(car index))
           ;running the loop without 0 index value
           (vars index rem-elem new temp
                 (cdr index) cont))
          (else
           ;;running the loop with the new values
           (vars given rem-elem first-elem temp
                (cdr index) (cons (sub1 (car index))cont)))))))

;; Side effects
;; IO(String)
;Mark
;F2 - I
(define (restart)
  (begin
    (set! hits 0); setting value to 0
    (set! plays 0); setting value to 0
    (set! failures 0); setting value to 0
    (set! partial-sol (map (λ (args) #\*) word-to-guess)); setting the next word back to *
    (set! total-hits (length word-to-guess)); setting value to len of selected word
    (set! total-failures 6); setting value to 6
    (set! word-to-guess (list-ref glossary (random (length glossary)))); pulling another word from the glossary after the first partial-sol pulled one
    (completed-game-status)));refresh game status


; F2 - II
(define (guess letter)
  (if (or (equal? failures total-failures); ends the game if you failed 6 attempts or
          (equal? hits total-hits));ends the game if you guessed all the letters
      (completed-game-status); returns game status
      (begin (set! plays (add1 plays)); + 1 each play
             (let ((hidden-word (indices word-to-guess letter)))
               (set! partial-sol (replace-indices partial-sol hidden-word letter)); replace * with letters user is entering
               (set! hits (+ hits (length hidden-word))); update no of hits based on the no of * replaced - bugged! 
               (if (equal? (length hidden-word) 0)
                 (set! failures (add1 failures)))); + 1 to failure for every non-hit letter
             (completed-game-status))));
   
;; F2 - III 
(define (solve string)
  (if (equal? string "");; entering empty string 
      "You entered empty word"
   (let ((var (string->list string)))
        (begin (for-each (λ (given-string)
                           (if
                            (member given-string partial-sol)
                            (void)
                            (guess given-string))) var);transform partial solution
               (set! hits (length word-to-guess))
               ;forcing the hits to be the same val of length of words
               ;return the game-status
               (completed-game-status)))))




;;
;; EXTRA -F3
; Kausar
(define (words-containing given-array given-character)
  (filter ; filtering out words which contains the given char 
   (lambda (lst-of-words)
            (member given-character lst-of-words))given-array))

;; Mark
;;creates the list of words which has the specific character inside it
(define (list-of-lists element1 element2)
  (cond ((null? element1) '())
        ((member (car element1) element2)
         (cons (car element1)
               (list-of-lists (cdr element1)
                              element2)))
        (else
         (list-of-lists
          (cdr element1)
          element2))))

;;chained function with the one above 
(define (words-containing-ext given-words char-hints)
  (if (null? given-words) '()
      (let ((specific-word-char
             (map (λ (charrs)
                    (words-containing given-words charrs))
                  char-hints)));take out words that the character only appears at
        (foldr list-of-lists
               (car specific-word-char)
               (cdr specific-word-char)))));returns list from right to left

;; invoking the hint
(define (sieve input-hint)
  (begin
    (map list->string ; return all as a string
              (words-containing-ext ; pass the input hint and glossary as param
               glossary input-hint)))); to pull the list of words which contains that specific character or list of letter

